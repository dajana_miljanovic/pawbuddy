<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = new \App\User;
        $user->name = "Admin";
        $user->email = "admin@admin.com";
        $user->password = bcrypt('admin');
        $user->save();

        \App\User::create(['name' => 'Vladimir Lelicanin', 'email' => 'vlelicanin@strategicpoint.rs', 'password' =>
            \Hash::make('admin123')]);

    }
}
