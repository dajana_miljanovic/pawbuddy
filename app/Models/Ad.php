<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Ad
 * @package App\Models
 * @version January 29, 2017, 6:18 pm UTC
 */
class Ad extends Model
{
    use SoftDeletes, Sluggable;

    public $table = 'ads';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'service_id',
        'title',
        'description',
        'residence',
        'kids',
        'car',
        'other_pets',
        'experience',
        'certified',
        'vet'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'service_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'residence' => 'boolean',
        'kids' => 'boolean',
        'car' => 'boolean',
        'other_pets' => 'string',
        'experience' => 'string',
        'certified' => 'boolean',
        'vet' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function service()
    {
        return $this->belongsTo(\App\Models\Service::class, 'service_id', 'id');
    }

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
