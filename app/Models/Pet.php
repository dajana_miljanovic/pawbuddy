<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pet
 * @package App\Models
 * @version January 29, 2017, 6:17 pm UTC
 */
class Pet extends Model
{
    use SoftDeletes;

    public $table = 'pets';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'name',
        'breed',
        'age',
        'gender',
        'social',
        'vaccinated',
        'healthy',
        'illness'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'name' => 'string',
        'breed' => 'string',
        'age' => 'string',
        'gender' => 'boolean',
        'social' => 'boolean',
        'vaccinated' => 'boolean',
        'healthy' => 'boolean',
        'illness' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }
}
