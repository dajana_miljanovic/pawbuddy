<?php

namespace App\Repositories;

use App\Models\Ad;
use InfyOm\Generator\Common\BaseRepository;

class AdRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'residence',
        'kids',
        'car',
        'other_pets',
        'experience',
        'certified',
        'vet'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Ad::class;
    }
}
