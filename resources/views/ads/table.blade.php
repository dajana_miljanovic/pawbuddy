<table class="table table-responsive" id="ads-table">
    <thead>
        <th>User Id</th>
        <th>Service Id</th>
        <th>Title</th>
        <th>Residence</th>
        <th>Kids</th>
        <th>Car</th>
        <th>Other Pets</th>
        <th>Experience</th>
        <th>Certified</th>
        <th>Vet</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($ads as $ad)
        <tr>
            <td>{!! $ad->user_id !!}</td>
            <td>{!! $ad->service_id !!}</td>
            <td>{!! $ad->title !!}</td>
            <td>{!! $ad->residence !!}</td>
            <td>{!! $ad->kids !!}</td>
            <td>{!! $ad->car !!}</td>
            <td>{!! $ad->other_pets !!}</td>
            <td>{!! $ad->experience !!}</td>
            <td>{!! $ad->certified !!}</td>
            <td>{!! $ad->vet !!}</td>
            <td>
                {!! Form::open(['route' => ['ads.destroy', $ad->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ads.show', [$ad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ads.edit', [$ad->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>