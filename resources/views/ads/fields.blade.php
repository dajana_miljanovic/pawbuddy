<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Service Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('service_id', 'Service Id:') !!}
    {!! Form::number('service_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Residence Field -->
<div class="form-group col-sm-12">
    {!! Form::label('residence', 'Residence:') !!}

</div>

<!-- Kids Field -->
<div class="form-group col-sm-12">
    {!! Form::label('kids', 'Kids:') !!}

</div>

<!-- Car Field -->
<div class="form-group col-sm-12">
    {!! Form::label('car', 'Car:') !!}

</div>

<!-- Other Pets Field -->
<div class="form-group col-sm-6">
    {!! Form::label('other_pets', 'Other Pets:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('other_pets', false) !!}
        {!! Form::checkbox('other_pets', '1', null) !!} 1
    </label>
</div>

<!-- Experience Field -->
<div class="form-group col-sm-6">
    {!! Form::label('experience', 'Experience:') !!}
    {!! Form::text('experience', null, ['class' => 'form-control']) !!}
</div>

<!-- Certified Field -->
<div class="form-group col-sm-12">
    {!! Form::label('certified', 'Certified:') !!}

</div>

<!-- Vet Field -->
<div class="form-group col-sm-12">
    {!! Form::label('vet', 'Vet:') !!}

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ads.index') !!}" class="btn btn-default">Cancel</a>
</div>
