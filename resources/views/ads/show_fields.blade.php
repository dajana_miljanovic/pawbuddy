<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ad->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $ad->user_id !!}</p>
</div>

<!-- Service Id Field -->
<div class="form-group">
    {!! Form::label('service_id', 'Service Id:') !!}
    <p>{!! $ad->service_id !!}</p>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:') !!}
    <p>{!! $ad->title !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $ad->description !!}</p>
</div>

<!-- Residence Field -->
<div class="form-group">
    {!! Form::label('residence', 'Residence:') !!}
    <p>{!! $ad->residence !!}</p>
</div>

<!-- Kids Field -->
<div class="form-group">
    {!! Form::label('kids', 'Kids:') !!}
    <p>{!! $ad->kids !!}</p>
</div>

<!-- Car Field -->
<div class="form-group">
    {!! Form::label('car', 'Car:') !!}
    <p>{!! $ad->car !!}</p>
</div>

<!-- Other Pets Field -->
<div class="form-group">
    {!! Form::label('other_pets', 'Other Pets:') !!}
    <p>{!! $ad->other_pets !!}</p>
</div>

<!-- Experience Field -->
<div class="form-group">
    {!! Form::label('experience', 'Experience:') !!}
    <p>{!! $ad->experience !!}</p>
</div>

<!-- Certified Field -->
<div class="form-group">
    {!! Form::label('certified', 'Certified:') !!}
    <p>{!! $ad->certified !!}</p>
</div>

<!-- Vet Field -->
<div class="form-group">
    {!! Form::label('vet', 'Vet:') !!}
    <p>{!! $ad->vet !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ad->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ad->updated_at !!}</p>
</div>

