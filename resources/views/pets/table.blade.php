<table class="table table-responsive" id="pets-table">
    <thead>
        <th>User Id</th>
        <th>Name</th>
        <th>Breed</th>
        <th>Age</th>
        <th>Gender</th>
        <th>Social</th>
        <th>Vaccinated</th>
        <th>Healthy</th>
        <th>Illness</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($pets as $pet)
        <tr>
            <td>{!! $pet->user_id !!}</td>
            <td>{!! $pet->name !!}</td>
            <td>{!! $pet->breed !!}</td>
            <td>{!! $pet->age !!}</td>
            <td>{!! $pet->gender !!}</td>
            <td>{!! $pet->social !!}</td>
            <td>{!! $pet->vaccinated !!}</td>
            <td>{!! $pet->healthy !!}</td>
            <td>{!! $pet->illness !!}</td>
            <td>
                {!! Form::open(['route' => ['pets.destroy', $pet->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('pets.show', [$pet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('pets.edit', [$pet->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>